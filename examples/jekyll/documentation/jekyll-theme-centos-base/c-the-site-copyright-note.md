### The site copyright note

The site copyright note is visible on the left-bottom side of all your pages.

<div class="bg-dark bg-image text-light p-3 mb-3">
{% include base/copyright.html %}
</div>

The `base/default` layout reads the site social networks data file from the
following location:

```
{%- raw -%}
src/_data/{{ site.component_data_dirname }}/copyright.yml
{% endraw %}
```

The configuration of `{% raw %}{{ site.component_data_dirname }}{% endraw %}`
variable takes place in the [site configuration
file](#the-site-configuration-file). When the copyright note data file you
configured does not exit in the [site directory
structure](#the-site-directory-structure), the `base/default` layout presents
the copyright note from the `src/_data/base/copyright.yml` file, available in
the theme directory structure.

The `_src/_data/base/copyright.yml` file has the following value:

```
{{ site.data.base.copyright }}
```

{:.table .table-bordered}
| Property | Description |
| -------- | ----------- |
| `author` | The site creator and owner of the copyright. |
| `legals` | List of links related to legal terms made of `text` and `link` elements. These items are presented on the right side of the copyright note. |

You can control the visibility of site copyright note changing the value
related to `with_copyright` [page configuration
variable](#the-page-configuration-variables).
