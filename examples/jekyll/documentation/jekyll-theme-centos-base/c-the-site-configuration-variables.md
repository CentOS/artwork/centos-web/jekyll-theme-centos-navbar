### The site configuration variables

The `base/default` layout requires you to configure the following site variables:

{:.table .table-bordered}
| Name | Default | Description |
| ---------------------- | ---------- | ----------- |
| `baseurl` | `""` | The base url where the site is hosted without trailing slash. E.g., `/artwork/centos-web/jekyll-theme-centos-base` |
| `defaults` | `[]` | [Jekyll Front Matter Defaults](https://jekyllrb.com/docs/configuration/front-matter-defaults/). To know more about default values, see [the site configuration file](#the-site-configuration-file). |
| `description` | `""` | A brief description about the site purpose. |
| `email` | `""` | The webmaster contact email address. |
| `component_data_dirname` | `"base"` | The directory name under `_data/` where the navigation bar data file lives. |
| `component_data_navbar` | `"navbar"` | The data file name where navigation bar content lives, under `site.component_data_dirname`. |
| `theme` | `""` | The theme name to use. E.g., `jekyll-theme-centos`. |
| `title` | `""` | The name of your site. |
| `url` | `""` | The site url without trailing slash. E.g., `https://centos.gitlab.io`|

The `base/default` layout also recognizes [Jekyll built-in site variables](https://jekyllrb.com/docs/variables/#site-variables).
