{% assign alert_colors = "info" | split: ", " %}

### Adding the site announcements

To add announcements to all site pages, edit [the site configuration
file](#the-site-configuration-file) and add your announcements under
`with_announcements`:

```Yaml
---
defaults:
 - scope:
     path: "" # an empty string here means all files in the project.
   values:
     # ----------------------------------------------------------------
     # Header
     # ----------------------------------------------------------------
     with_announcements:
       {% for alert_color in alert_colors -%}
       {%- capture alert_title -%}
       This is a {{ alert_color }} title
       {%- endcapture -%}
       {%- capture alert_content -%}
       This is a {{ alert_color }} announcement content.
       {%- endcapture -%}
       - title: "{{ alert_title }}"
         color: "{{ alert_color }}"
         content: "{{ alert_content }}"
       {%- endfor %}
```
