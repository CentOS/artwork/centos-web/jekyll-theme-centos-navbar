### Regenerating the site `public` directory

To regenerate the public site from source files, run jekyll build command using
one of the
[jekyll-theme-centos](https://gitlab.com/CentOS/artwork/centos-web/jekyll-theme-centos)
container images:

```Shell
podman run --rm \
 -v $$PWD/public:/public \
 -v $$PWD/src:/site \
 --name jekyll-theme-centos \
 registry.gitlab.com/centos/artwork/centos-web/jekyll-theme-centos:latest \
 bundle exec jekyll build --config /site/_config.yml -s /site -d /public
```

In this example the `jekyll build` command uses the `latest` container image of
[jekyll-theme-centos](https://gitlab.com/CentOS/artwork/centos-web/jekyll-theme-centos)
and renders your site with it. This image contains the theme layouts, includes,
data files, auxiliary images, and tools you need to build your site using
`jekyll-theme-centos` theme.

{:class="alert alert-danger"}
**CAUTION:** Using `latest` when building the site may introduce unexpected
visual changes. Always use a specific
[release](https://gitlab.com/CentOS/artwork/centos-web/jekyll-theme-centos/-/releases){:.alert-link}
version to have a consistent visual presentation between builds.
