{% assign title = "Example Title." %}
{% assign color = "primary" %}
{% assign content = 'This is the announcement content with <a href="#" class="alert-link">link</a> for testing purposes.' %}

### The page announcements

The `base/default` layout uses the `with_announcements` configuration variable
and `_includes/base/announcements.html` template file to present announcements
on the page main section, just below the breaking news section.

The announcements are less relevant than breaking news. You use announcements
to alert your users about peculiarities affecting the page content. For
example, when a page content gets outdated, or it is being worked on and you
want to prevent users from relaying entirely on it during the edition process.

#### Default format

The announcements have the following visual format:

{% include base/announcements.html
  title=title
  content=content
  color=color
%}

```
---
with_announcements:
  - title: "{{ title }}"
    content: '{{ content }}'
    color: "{{ color }}"
```

{:.table .table-bordered}
| Property | Description |
| --------- | ----------- |
| `title` | The announcement title. |
| `content` | The announcement content. |
| `color` | The announcement color. Possible options include `primary` (default), `secondary`, `info`, `warning`, `danger`, `success`, `dark`, `light`. |

#### Custom formats

{% assign colors = "secondary, info, warning, danger, success, dark, light" | split: ", " %}

{% for color in colors %}
{% include base/announcements.html
  title=title
  content=content
  color=color
%}

```
---
with_announcements:
  - title: "{{ title }}"
    content: '{{ content }}'
    color: "{{ color }}"
```
{% endfor %}
