### Adding links to site navigation bar

To add links to site navigation bar, add the following YAML structure to [site
navigation data file](#the-site-navigation-bar):

```Yaml
---
- name: "Simple Link"
  icon: "fa-solid fa-link"
  link: "#"
  menu: []
  visible_on: ["navbar"]
```

{:.table .table-bordered}
| Parameter | Description |
| ------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `name` | The name of the link. |
| `icon` | The icon class rendered on the left side menu name. Possible options to use use here are limited to [fontawesome freely distributed icons](https://fontawesome.com/search?o=r&m=free). |
| `link` | The URL associated to the link name. Here, URLs are relative to `site.baseurl`. |
| `menu` | Must be an empty list. For example `[]`. |
| `visible_on` | Must be `["navbar"]`. Simple links are visible in the navigation bar only. Simple links are not presented in the footer shortcuts section. |
