### Adding tables

When you are adding content tables in your pages, check you provide the
`{:.table .table-bordered}` class definition on top of your Markdown table
definitions. This allows your table to be properly rendered using Bootstrap
table style.

Example:

```
{:.table .table-bordered}
| Name | Default | Description |
| ---- | ------- | ----------- |
| a    | b       | c           |
| d    | e       | f           |
```

{:.table .table-bordered}
| Name | Default | Description |
| ---- | ------- | ----------- |
| a | b | c |
| d | e | f |

When your content tables are lengthy, and you find it hard to find information
inside them, consider adding a search box to improve its accessibility.
