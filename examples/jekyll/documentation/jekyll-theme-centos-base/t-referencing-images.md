### Referencing images

To reference images in you pages, do the following:

1. Use the `base/image.html` template when you are [adding images](#adding-images).

1. Use Markdown to create the reference link. In the address part, add the
   value of the `alt` property from the image you want to reference to. When
   you do so, make the value lowercase, and replace all occurrences of space
   (` `) and slash (`/`) characters with dash (`-`) characters. All dot (`.`)
   characters are removed from the final id value.

   Example:

   ```
   [The CentOS Artistic Motif](#the-centos-artistic-motif)
   ```

   [The CentOS Artistic Motif](#the-centos-artistic-motif)
