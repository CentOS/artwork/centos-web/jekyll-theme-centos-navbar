### Adding the site breaking news

To add breaking news to all site pages, edit the site configuration file and
add your announcements under `with_breakingnews`:

```Yaml
---
defaults:
 - scope:
     path: "" # an empty string here means all files in the project.
   values:
     # ----------------------------------------------------------------
     # Header
     # ----------------------------------------------------------------
     with_breakingnews:
       - title: "Example Title!"
         content: "This is the breaking news for testing purposes. "
         url: "#"
         color: "primary"
```
