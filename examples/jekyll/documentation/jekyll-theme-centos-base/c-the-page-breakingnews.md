### The page breaking news

The `base/default` layout uses the `with_breakingnews` configuration variable
and `_includes/base/breakingnews.html` template file to present breaking news.

The breaking news is way more relevant than announcement. It is always visible
on the page, and the user cannot dismiss it once configured (e.g., there is not
close button). Complementary, the breaking news container can present a "Known
more" button to link a page describing the breaking news in more details.

Only use breaking news when you need to communicate important CentOS events,
end-of-life notes, availability of new major releases, and critical issues
related to CentOS Project itself.

#### Default presentation

{% assign content = 'This is a high impact message for testing purposes.' %}

{% include base/breakingnews.html
content=content
url=url
color="primary"
%}

```
---
with_breakingnews:
  - content: "{{ content }}"
    url: "{{ url }}"
    color: "{{ color }}"
```

When you are adding content to breaking news container, consider using a
maximum of two visible lines. Having more than two visible lines in the
breaking news container will expand its height to greater values than the
maximum value (38px) used to calculate the `scroll-margin-top` property. In
such case, the content headings will be hidden under the breaking news
container when you access them through the page's table of content. To present
effective breaking news, keep your message short, one-line if possible, and use
the "know more" button URL to link details.

#### Custom presentation

{% assign colors = "secondary, info, warning, danger, success, dark, light" | split: ", " %}

{% for color in colors %}
{% include base/breakingnews.html
  content=content
  color=color
  url=url
%}

```
---
with_breakingnews:
  - content: '{{ content }}'
    color: "{{ color }}"
    url: "{{ url }}"
```
{% endfor %}
