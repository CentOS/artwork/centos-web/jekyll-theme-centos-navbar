{% assign alert_colors = "info" | split: ", " %}

### Adding the page announcements

To add announcements to one single page, add the `with_announcements`
configuration variable to that page front matter along the announcements you
want to add as value.

```
---
title: This is a page with announcements

with_announcements:
  {% for alert_color in alert_colors -%}
  {%- capture alert_title -%}
  This is a {{ alert_color }} title
  {%- endcapture -%}
  {%- capture alert_content -%}
  This is a {{ alert_color }} announcement content.
  {%- endcapture -%}
  - title: "{{ alert_title }}"
    color: "{{ alert_color }}"
    content: "{{ alert_content }}"
  {%- endfor %}
---

Page content here.
```
