### The site configuration scopes

The site configuration scopes are relevant for configuration variables like
`with_announcements` and `with_breakingnews`, where you need to control the
visibility of important messages accross your site pages.

The site configuration scopes are "site-level" and "page-level."  You define
site-level configuration variables in [the site configuration
file](#the-site-configuration-file) and page-level configuration variables in
the front-matter section of individual site pages. When the same configuration
variable is defined in both site-level and page-level scopes, the value set in
the page-level scope overrides the value set in site-level scope.

At operation level, the changes related to configuration variables in both
site-level and page-level scopes require you to rebuild the site files running
the `jekyll build` command. When you provide the `--watch` flag to `jekyll
build` command, it keeps watching for site changes and auto-regenerate all the
site files when one happens. When the change is in the site configuration file,
however, the `--watch` flag does not auto-regenerate the site files and you
need to run `jekyll build` command again for changes to take effect.
