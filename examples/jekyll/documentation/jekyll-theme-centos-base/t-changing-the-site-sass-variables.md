### Changing Sass variables

To change the [Sass variables](#sass-variables), do the following:

1. Identify the sass variable you want to change. You can do this
   checking [Bootstrap's default scss variables](https://github.com/twbs/bootstrap/blob/main/scss/_variables.scss).
   This file contains all the variables you can customize.
   definition.
1. Create the `_sass/base/_variables.scss` file in your site directory structure.
1. Make your changes in the `_sass/base/_variables.scss` file.
