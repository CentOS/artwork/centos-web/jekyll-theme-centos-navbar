### The site navigation bar

The site navigation bar provides an always-visible set of links on the very top
of all your pages. In the navigation bar space, you can configure simple links,
or menu links limited to one level.

The `base/default` layout reads the site navigation bar data file from the
following location:

```
{%- raw -%}
src/_data/{{ site.component_data_dirname }}/{{ site.component_data_navbar }}.yml
{% endraw %}
```

The configuration of both `{% raw %}{{ site.component_data_dirname }}{% endraw %}`
and `{% raw %}{{ site.component_data_navbar }}{% endraw%}` variables take place
in the [site configuration file](#the-site-configuration-file). When the site
navigation bar data file you configured does not exit in the [site directory
structure](#the-site-directory-structure), the site navigation bar falls back
to `src/_data/base/navbar.yml` file, available in the theme directory
structure.

The `_src/_data/base/navbar.yml` file has the following value:

```
{{ site.data.base.navbar }}
```

The site navigation bar data file comprises
[links](#adding-links-to-site-navigation-bar) and
[menus](#adding-menus-to-site-navigation-bar).
