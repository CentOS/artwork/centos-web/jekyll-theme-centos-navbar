### Adding menus to site navigation bar

To add menus to site navigation bar, add the following YAML structure to [site
navigation data file](#the-site-navigation-bar):

```Yaml
---
- name: "Menu Link"
  icon: "fa-solid fa-square-plus"
  menu:
    - name: "First parent page"
      link: "#"
    - name: "Second parent page"
      link: "#"
  visible_on: ["navbar", "footer"]
```

{:.table .table-bordered}
| Parameter | Description |
| ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `name` | The name of the menu where links are organized. |
| `icon` | The icon class rendered on the left side menu name. Possible options to use use here are limited to [fontawesome freely distributed icons](https://fontawesome.com/search?o=r&m=free). |
| `menu` | Must be a non-empty list holding one or more entries of `{"name": "", "link": ""}` dictionary. In this dictionary, `name` is the name of the link and `link` is the URL associated to the link name. The link can be absolute or relative. |
| `visible_on` | Must be `["navbar", "footer"]`. Menu links are visible in the navigation bar and in the footer shortcuts section. |
