### The site directory structure

The site configuration starts with the following directory structure:

```Shell
.
├── README.md
├── public/
└── src/
   ├── _config.yml
   └── index.md
```

Where:

{:.table .table-bordered}
| File | Description |
| ---- | ----------- |
| `README.md` | File describing the site purpose. |
| `public` | Directory where the final site is published. This is the directory you expose to your audience. |
| `src/_config.yml` | The site configuration file. Here is where you set your site global defaults. |
| `src/index.md` | The site home page. |
