### The site fonts

The `base/default` layout uses the [Montserrat](https://fonts.google.com/specimen/Montserrat/about?query=montserrat)
and
[Source Code Pro](https://fonts.google.com/specimen/Source+Code+Pro/about?query=source+code+pro)
fonts, both available under the
[Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).

The `base/default` layout implements this configuration changing the
following files:

`_includes/base/head.html`:

```Html
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Source+Code+Pro:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Source+Code+Pro:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap">
```

`_sass/base/_variables.scss`:

```SCSS
$font-family-sans-serif: "Montserrat", sans-serif;
$font-family-monospace: "Source Code Pro", monospace;
```
