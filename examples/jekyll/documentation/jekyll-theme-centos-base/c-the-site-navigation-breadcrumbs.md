### The site navigation breadcrumbs

The [`base/default` layout](#the-page-basedefault-layout) provides the
`base/breadcrumbs.html` component to build a line of page links automatically,
using the site directory structure as reference.  You can find the site
navigation breadcrumbs on each page, between the page announcements and the
page title.

The `base/breadcrubms.html` component describes the page location inside the
site hierarchy using links you can access quickly. The breadcrumbs presentation
starts with the [Home]({{ site.url }}{{ site.baseurl }}) page link and goes all
the way down the site hierarchy linking intermediate pages until reaching the
page you are currently reading, which is not linked. The breadcrumbs create a
visual map that guides you while seeking specific information. Both breadcrumbs
and [site navigation flow](#site-navigation-flow) schema work together to
deliver a consistent navigation experience.

Example:

```
.
├── _config.yml
└── documentation.md
```

{% include base/breadcrumbs.html %}
