### Adding images

To add images, use the `base/image.html` template.

```
{%- raw -%}
{% include base/image.html file="centos-motif.png"
alt="The CentOS Artistic Motif"
caption="The CentOS Artistic Motif"
extraclasses="bg-dark"
%}
{% endraw -%}
```

{% include base/image.html file="centos-motif.png"
alt="The CentOS Artistic Motif"
caption="The CentOS Artistic Motif"
extraclasses="bg-dark"
%}

{:.table .table-bordered}
| Property | Type | Description |
| -------- | ------- | ----------- |
| `file` | `""` | The path to the image file. This path is relative to `/assets/img/`. |
| `alt` | `""` | The alterntive text passed to `alt` property in `<img>` HTML element. The `alt` property does not allow HTML code as value. |
| `caption` | `""` | The text passed to `<figcaption>` HTML element. The `caption` property does allow HTML code as value. |
| `extraclasses` | `""` | Extra classes added to `<img>` HTML element. Supported classes include all Bootstrap classes that make sense here. For example, you can use this property to provide the background color of semi-transparent images. |

{:.alert .alert-danger}
**CAUTION:** Do not duplicate the value of `alt` properties when you use the
`base/image.html` template to create more than one image in the same page. This
causes figures to have duplicated `id` properties and make figure references to
fail.
