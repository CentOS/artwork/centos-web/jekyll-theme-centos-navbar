### Changing the site navigation bar

You can customize the navigation bar data file path using the
`site.component_data_dirname` and `site.component_data_navbar` variables in the
site configuration file. This is useful when you build your site from different
configuration files and want to use a different navigation bar data file for
each of them. For example, you have one configuration file for `dev`,
`staging`, and `prod` environment where the navigation links to be adjusted
according differences related to these environments.

To change the site navigation bar of your site, do the following:

1. Edit the `src/_config.yml` file, and set the `{% raw %}{{
site.component_data_dirname }}{% endraw %}` and `{% raw %}{{
site.component_data_navbar }}{% endraw %}` variables.

1. Create the `{% raw %}src/_data/{{ site.component_data_dirname }}/{{
   site.component_data_navbar }}{% endraw %}.yml` data file in your site directory structure.

1. Edit `{% raw %}src/_data/{{ site.component_data_dirname }}/{{
   site.component_data_navbar }}{% endraw %}.yml` file to configure
   [links](#navigation-bar-link-configuration) and
   [menus](#navigation-bar-menu-configuration).
