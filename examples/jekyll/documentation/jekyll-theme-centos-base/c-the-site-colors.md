### The site colors

The `base/default` layout changes the `_sass/base/_variables.scss` file to
customize Boostrap default color theme using the CentOS Colors. The changes
introduced affect the following classes:

<div class="d-flex justify-content-between bg-dark p-3"><code><strong>.bg-dark</strong></code> <span class="text-bg-dark">Dark</span> <code><strong>.text-bg-dark</strong></code></div>
<div class="d-flex justify-content-between bg-primary p-3"><code><strong>.bg-primary</strong></code> <span class="text-bg-primary">Primary</span> <code><strong>.text-bg-primary</strong></code></div>
<div class="d-flex justify-content-between bg-secondary p-3 mb-3"><code><strong>.bg-secondary</strong></code> <span class="text-bg-secondary">Secondary</span> <code><strong>.text-bg-secondary</strong></code></div>

Other Bootstrap color classes remain intact:

<div class="d-flex justify-content-between bg-light p-3"><code><strong>.bg-light</strong></code> <span class="text-bg-light">Light</span> <code><strong>.text-bg-light</strong></code></div>
<div class="d-flex justify-content-between bg-danger p-3"><code><strong>.bg-danger</strong></code> <span class="text-bg-danger">Danger</span> <code><strong>.text-bg-danger</strong></code></div>
<div class="d-flex justify-content-between bg-success p-3"><code><strong>.bg-success</strong></code> <span class="text-bg-success">Success</span> <code><strong>.text-bg-success</strong></code></div>
<div class="d-flex justify-content-between bg-warning p-3"><code><strong>.bg-warning</strong></code> <span class="text-bg-warning">Warning</span> <code><strong>.text-bg-warning</strong></code></div>
<div class="d-flex justify-content-between bg-info p-3 mb-3"><code><strong>.bg-info</strong></code> <span class="text-bg-info">Info</span> <code><strong>.text-bg-info</strong></code></div>

Background gradients:

<div class="d-flex justify-content-between bg-gradient-primary p-3 mb-3"><code><strong>.bg-gradient-primary</strong></code><span class="h4 p-0 m-0"><i class="fa-solid fa-flask-vial"></i></span> <span class="h4 p-0 m-0">Section Name</span> </div>
<div class="d-flex justify-content-between bg-gradient-secondary p-3 mb-3"><code><strong>.bg-gradient-secondary</strong></code><span class="h4 p-0 m-0"><i class="fa-solid fa-flask-vial"></i></span> <span class="h4 p-0 m-0">Section Name</span> </div>

Background gradients nested with gradients:

<div class="bg-gradient-primary p-3 mb-3"><code><strong>.bg-gradient-primary</strong></code><div class="mt-3 bg-gradient-secondary p-3"><code><strong>.bg-gradient-secondary</strong></code></div> </div>
<div class="bg-gradient-secondary p-3 mb-3"><code><strong>.bg-gradient-secondary</strong></code><div class="mt-3 bg-gradient-primary p-3"><code><strong>.bg-gradient-primary</strong></code></div> </div>

Background gradient nested with plain color:

<div class="bg-gradient-primary p-3 rounded mb-3"><code><strong>.bg-gradient-primary</strong></code><div class="mt-3 bg-body p-3 rounded"><code><strong>.bg-body</strong></code></div> </div>
<div class="bg-gradient-secondary p-3 rounded mb-3"><code><strong>.bg-gradient-secondary</strong></code><div class="mt-3 bg-body p-3 rounded"><code><strong>.bg-body</strong></code></div> </div>

Text gradients:

<div class="bg-body p-3 rounded">
  <code><strong>.bg-body</strong></code>
  <code><strong>.text-gradient-primary</strong></code>
  <div class="display-6 text-gradient-primary my-3 text-shadow">Community Enterprise Operating System</div>
</div>

<div class="bg-dark p-3 rounded">
  <code><strong>.bg-dark</strong></code>
  <code><strong>.text-gradient-primary</strong></code>
  <div class="display-6 text-gradient-primary my-3">Community Enterprise Operating System</div>
</div>

<div class="bg-body p-3 rounded">
  <code><strong>.bg-body</strong></code>
  <code><strong>.text-gradient-secondary</strong></code>
  <div class="display-6 text-gradient-secondary my-3">Community Enterprise Operating System</div>
</div>

<div class="bg-dark p-3 rounded">
  <code><strong>.bg-dark</strong></code>
  <code><strong>.text-gradient-secondary</strong></code>
  <div class="display-6 text-gradient-secondary my-3">Community Enterprise Operating System</div>
</div>
