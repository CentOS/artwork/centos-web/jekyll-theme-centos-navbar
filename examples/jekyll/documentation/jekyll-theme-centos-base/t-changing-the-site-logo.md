### Changing the site logo

To change the site logo, do the following in your site directory structure:

1. Copy the logo image in SVG format somewhere under `src/assets/img/` directory
   (e.g., `src/assets/img/site-logo.svg`.

1. Edit your site configuration file and set the `with_logo:` variable to point
   to your new logo image (e.g., `site-logo.svg`) relatively to `assets/img` path.

   ```
   ---
   defaults:
     - scope:
         path: "" # an empty string here means all files in the project.
       values:
         layout: "base/default"
         with_logo: "site-logo.svg"
   ```
