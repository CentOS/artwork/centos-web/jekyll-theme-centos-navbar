### The page `base/default` layout

The page `base/default` layout is the default layout for content presentation
in `jekyll-theme-centos` theme. It stands on top of [Bootstrap v5.3](https://getbootstrap.com/docs/5.3/getting-started/introduction/)
components and allows page customization by means of _"site configuration
variables"_, _"page configuration variables"_ and _"site data files"_.

The page `base/default` layout has the following structure:

{% include base/image.html
file="base/screenshot-base-default-layout.png"
alt="The page base/default layout"
caption="The page <code>base/default</code> layout structure."
%}
