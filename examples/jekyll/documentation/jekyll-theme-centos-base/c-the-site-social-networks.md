### The site social networks

The site social networks are visible on the right-bottom side of all your
pages.

<div class="bg-dark bg-image text-light p-3 mb-3">
{% include base/finale.html %}
</div>

The `base/default` layout reads the site social networks data file from the
following location:

```
{%- raw -%}
src/_data/{{ site.component_data_dirname }}/social.yml
{% endraw %}
```

The configuration of `{% raw %}{{ site.component_data_dirname }}{% endraw %}`
variable takes place in the [site configuration
file](#the-site-configuration-file). When the social networks data file you
configured does not exit in the [site directory
structure](#the-site-directory-structure), the `base/default` layout presents
social networks form `src/_data/base/social.yml` file, available in the theme
directory structure.

The `_src/_data/base/social.yml` file has the following value:

```
{{ site.data.base.social }}
```

{:.table .table-bordered}
| Property | Description |
| -------- | ----------- |
| `name` | The name of the social network you want to present. |
| `icon` | The brand icon of the social network you want to present. Available icons are limited to [fontawesome free brands](https://fontawesome.com/search?o=r&m=free&f=brands) only. |
| `link` | The URL of the social network you want to present. |

You can control the visibility of social networks changing the value related to
`with_socials` [page configuration variable](#the-page-configuration-variables).
