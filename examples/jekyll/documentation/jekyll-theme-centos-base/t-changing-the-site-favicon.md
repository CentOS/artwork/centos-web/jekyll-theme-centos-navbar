### Changing the site favicon

To change the site favicon, ensure the following directory structure and file
names exist in your site:

```
src/
├── site.webmanifest
└── assets/icons
    ├── android-chrome-192x192.png
    ├── android-chrome-512x512.png
    ├── apple-touch-icon.png
    ├── favicon-16x16.png
    ├── favicon-32x32.png
    ├── favicon.ico
    └── favicon.svg
```

You can create these files using your site logo SVG file and an [online favicon
generator](https://favicon.io/). When you do so, review the content of
`site.webmanifest` file to be sure it has the correct paths to find the images
in your site. You can use the following `site.webmanifest` example as
reference:

```json
{
  "name": "",
  "short_name": "",
  "icons": [
    { "src": "/assets/icons/android-chrome-192x192.png", "sizes": "192x192", "type": "image/png" },
    { "src": "/assets/icons/android-chrome-512x512.png", "sizes": "512x512", "type": "image/png" }
  ],
  "theme_color": "#ffffff",
  "background_color": "#ffffff",
  "display": "standalone"
}
```
