### Changing the site artistic motif

To change the site artistic motif, do the following in your site directory structure:

1. Copy the artistic motif in PNG format somewhere under `assets/img/`
   directory (e.g., `assets/img/site-motif.png`.

1. Add the `with_motif` variable to your site configuration file and change its
   value to have the path of the artistic motif image you copied (e.g.,
   `assets/img/site-motif.png`.)

When you are producing a new site artistic motif, consider the following
recommendations:

{:.table .table-bordered}
| Property | Description |
| ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Size | The image should look good in both small and large screens. Consider using an image of 1920x1080 pixels. |
| Contrast | The artistic motif image is used as background on headers. It must provide a high contrast ratio (e.g., AAA) image for white text presented over it. |
| Transparency | Consider using semi-transparent images on top of `#200735` (the dark color in the color schema) plain color. This allows the image to adapt itself to different background colors. In this case, consider converting the image mode from RGB to greyscale. |
| Position | The attention point in the image must be on the right side or presented in a way that doesn't make text on top difficult to read. |
| Optimization | The image must load fast. So, make it small. Always optimize the final artistic motif PNG file using the `pngquant` tool. |

{% include base/image.html
file="centos-motif.png"
alt="Semi-transparent artistic motif on white background."
caption="Semi-transparent artistic motif on white background."
%}

{% include base/image.html
file="centos-motif.png"
alt="Semi-transparent artistic motif on dark background"
caption="Semi-transparent artistic motif on dark background."
extraclasses="bg-dark"
%}

<div class="bg-image bg-dark text-light rounded p-5 mb-3"{% if page.with_motif != "" %} style="background-image: url('{{ site.url }}{{ site.baseurl }}/assets/img/{{ page.with_motif }}');"{% endif %}>
{% if page.with_title != false -%}
<div class="h1">{{ page.title }}</div>
{% endif -%}
{% if page.with_preamble != false -%}
<div class="lead">{{ page.title_lead }}</div>
{% endif %}
</div>

Semi-transparent artistic motif on dark background with white text on top.
