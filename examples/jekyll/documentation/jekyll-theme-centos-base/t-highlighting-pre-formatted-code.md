### Highlighting pre-formatted code

The `base/default` layout uses [highlightjs](https://highlightjs.org/) to
highlight code blocks in content. By default, highlight of pre-formatted code
is disabled.

To enable pre-formatted code highlight, add the `with_highlight: stackoverflow-light` in the page front matter.

```
---
title: Page with code blocks highlighted

with_highlight: stackoverflow-light
---

Page content here.
```

{:class="alert alert-warning"}
Use one unique [theme](https://highlightjs.org/examples){:.alert-link} name
across in all the site pages to achieve high visual consistency.

{:class="alert alert-warning"}
Enable highlight of pre-formatted code where you really need it. By doing so,
you reduce the amount of code your page needs to load and so you make it load
faster.
