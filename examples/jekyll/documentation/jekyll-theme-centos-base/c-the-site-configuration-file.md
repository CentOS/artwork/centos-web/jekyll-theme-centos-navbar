### The site configuration file

The site configuration file is `src/_config.yml` and the `base/default` layout
requires the site to have, at least, the following configuration variables in
it:

```Yaml
---
# Welcome to Jekyll!
#
# This config file is meant for settings that affect your whole blog, values
# which you are expected to set up once and rarely edit after that. If you find
# yourself editing this file very often, consider using Jekyll's data files
# feature for the data you need to update frequently.
#
# For technical reasons, this file is *NOT* reloaded automatically when you use
# 'bundle exec jekyll serve'. If you change this file, please restart the
# server process.
#
# If you need help with YAML syntax, here are some quick references for you:
# https://learn-the-web.algonquindesign.ca/topics/markdown-yaml-cheat-sheet/#yaml
# https://learnxinyminutes.com/docs/yaml/

title: jekyll-theme-centos-base
description: "HTML templates, YAML files and images related to jekyll-theme-centos-base."
email: areguera@centosproject.org
url: "https://centos.gitlab.io"
baseurl: "/artwork/centos-web/jekyll-theme-centos-base"

# site.theme - Set the name of the site theme. The theme versions are
# controlled using jekyll-theme-centos container images.  These images are
# versioned and provide all you need to build jekyll sites with
# jekyll-theme-centos.
theme: "jekyll-theme-centos"

# site.component_data_dirname - Set the directory name where the navbar data files
# is stored. For example, if the navbar file is at `_data/base/navbar.yml', the
# value you need to provide is `base'.
component_data_dirname: "base"

# site.component_data_navbar - Set the name of the navbar data file. For
# example, if the navbar file is at `_data/base/navbar.yml', the value you need
# to provide is `navbar', without the extension.
component_data_navbar: "navbar"

defaults:
  - scope:
      path: "" # an empty string here means all files in the project.
    values:
      layout: "base/default"
      # ----------------------------------------------------------------
      # Nav
      # ----------------------------------------------------------------
      with_logo: "centos-whitelogo.svg"
      with_manifestation: "jekyll-theme-centos-base"
      # ----------------------------------------------------------------
      # Header
      # ----------------------------------------------------------------
      with_breakingnews: []
      with_announcements: []
      with_motif: "centos-motif.png"
      with_title: true
      with_preamble: true
      # ----------------------------------------------------------------
      # Main
      # ----------------------------------------------------------------
      with_breadcrumbs: true
      with_toc: false
      with_artwork: false
      with_content: true
      # ----------------------------------------------------------------
      # Footer
      # ----------------------------------------------------------------
      with_footer: true
      with_shortcuts: true
      with_sponsors: false
      with_social: true
      with_finale: true
      with_copyright: true
      # ----------------------------------------------------------------
      # Script (also changes Head, to manage script-related stylesheets)
      # ----------------------------------------------------------------
      with_highlight: false
      with_datatables: false

plugins:
  - jekyll-feed
  - jekyll-toc

permalink: /:path/:basename/
```
