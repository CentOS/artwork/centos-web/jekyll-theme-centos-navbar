### Adding contextual feedback

To add contextual feedback in your pages, do the following:

1. Create a paragraph.
1. Add the `{:}` line before the paragraph. Don't add empty lines between `{:}` and the paragraph.
1. Add Bootstrap classes related Alerts inside `{:}`.

Examples:

```Markdown
{:.alert .alert-primary}
**CentOS Recommends:** This is a CentOS recommendation message with [link](#){:.alert-link} for testing purposes.
```

{:.alert .alert-primary}
**CentOS Recommends:** This is a CentOS recommendation message with [link](#){:.alert-link} for testing purposes.

```Markdown
{:.alert .alert-secondary}
**CentOS Tip:** This is a CentOS Tip message with [link](#){:.alert-link} for testing purposes.
```

{:.alert .alert-secondary}
**CentOS Tip:** This is a CentOS Tip message with [link](#){:.alert-link} for testing purposes.

```Markdown
{:.alert .alert-warning}
**WARNING:** This is a warning message with [link](#){:.alert-link} for testing purposes.
```

{:.alert .alert-warning}
**WARNING:** This is a warning message with [link](#){:.alert-link} for testing purposes.

```Markdown
{:.alert .alert-danger}
**CAUTION:** This is a caution message with [link](#){:.alert-link} for testing purposes.
```

{:.alert .alert-danger}
**CAUTION:** This is a caution message with [link](#){:.alert-link} for testing purposes.

```Markdown
{:.alert .alert-info}
**INFO:** This is an info message with [link](#){:.alert-link} for testing purposes.
```

{:.alert .alert-info}
**INFO:** This is an info message with [link](#){:.alert-link} for testing purposes.

```Markdown
{:.alert .alert-light}
**NOTE:** This is a note message with [link](#){:.alert-link} for testing purposes.
```

{:.alert .alert-light}
**NOTE:** This is a note message with [link](#){:.alert-link} for testing purposes.

```Markdown
{:.alert .alert-success}
**SUCCESS:** This is a success message with [link](#){:.alert-link} for testing purposes.
```

{:.alert .alert-success}
**SUCCESS:** This is a success message with [link](#){:.alert-link} for testing purposes.
