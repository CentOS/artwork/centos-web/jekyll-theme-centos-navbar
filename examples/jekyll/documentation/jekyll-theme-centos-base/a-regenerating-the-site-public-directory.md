{:.alert .alert-warning}
**NOTE:** When you make changes in the site configuration file or a page, those
changes are not automatically visible in the public site. To make those changes
visible, you must [regenerate the public site
files](#regenerating-the-site-public-directory){:.alert-link} first.
