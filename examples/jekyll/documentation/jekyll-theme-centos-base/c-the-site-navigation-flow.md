### The site navigation flow

The site navigation flow establishes the progression in which you find the
information you want in the site. The progressing is as follows: use an
inverted tree structure to cover topics in less detail up-side and in more
details down-side. In this structure, down-side content is always related to
up-side content. When the content is not related, a new top-level page should
be created to cover the topic there. The breadcrumbs section on each page
provides a way to go up in the hierarchy.

The following illustration presents the progression followed to go from one
top-level page (`A`) to a down-side page (`C`) passing through a content page (`B`)
with links to related pages.

{% include base/image.html file="base/the-site-navigation-flow.webp"
caption="The site navigation flow"
alt="The site navigation flow"
%}

When you expand a content page creating other pages, it becomes an index page
for those pages it links to. It presents both content and links (e.g., See `A`,
and `B` in the illustration above). In these cases, the presentation order is
content first and index of links later.

#### Categories in content page

The content page is where you define the page `categories`. Consider the
following example setting two categories named `documentation` and
`jekyll-theme-centos-base` in the page front matter section:

```
---
title: Documentation

categories: ["documentation", "jekyll-theme-centos-base"]
---
```

#### Categories in index page

The index page is where you present links to other content-related pages. The
relation is established filtering the page categories in the `site.pages`
variable.

Example:

```
{%- raw -%}
{% assign pages = site.pages | where: "categories", "documentation" | where: "categories", "jekyll-theme-centos-base" %}
{% include base/pages.html pages=pages %}
{% endraw -%}
```

{% assign pages = site.pages | where: "categories", "documentation" | where: "categories", "jekyll-theme-centos-base" %}
{% include base/pages.html pages=pages %}
