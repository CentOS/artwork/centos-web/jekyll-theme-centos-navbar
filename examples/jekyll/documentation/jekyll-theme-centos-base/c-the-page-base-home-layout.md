### The page `base/home` layout

{% include base/image.html
file="base/screenshot-base-home-layout.png"
alt="The page base/home layout"
caption="The page <code>base/home</code> layout structure."
%}
