### Adding tables with search box

The `base/default` layout uses [DataTables](https://datatables.net/) files to
present your content tables with handy features, including search box and
content pagination. By default, DataTables is disabled.

{:class="alert alert-warning"}
Enable DataTables where you really need it. By doing so, you reduce the amount
of code your page needs to load and so makes it load faster.

To enable search box in your content tables, do the following:

1. Enable DataTables in the page you plan to use it. You do this adding the
   `with_datatables: true` parameter to your page front matter.

   ```
   ---
   title: Page with datatables enabled

   with_datatables: true
   ---
   ```

1. Attach the `dataTable` class to content tables you want to have the feature
   enabled on.

   ```
   {:.table .table-bordered .dataTable}
   | Name | Default | Description |
   | ---- | ------- | ----------- |
   | a    | b       | c           |
   | d    | e       | f           |
   ```

   Example:

   {:.table .table-bordered .dataTable}
   | Name | Default | Description |
   | ---- | ------- | ----------- |
   | a | b | c |
   | d | e | f |
