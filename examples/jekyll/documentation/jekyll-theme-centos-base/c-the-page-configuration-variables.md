### The page configuration variables

The `base/default` layout requires you to configure the following page variables:

{:.table .table-bordered .dataTable}
| Name | Default | Section | Description |
| -------------------- | ------------------------ | -------- | ----------- |
| `with_breakingnews` | `[]` | `header` | When the value is an empty list (`[]`), the breaking news section is not rendered. When the value is a non-empty list (`[{"title": "", "content":"", "url":""}]`), the breaking news is rendered, one for each item in the list. All breaking news properties in the dictionary are optional. However, you probably want to at least `title` to provide a meaningful message. |
| `with_announcements` | `[]` | `main` | When the value is an empty list (`[]`), the announcements section is not rendered. When the value is a non-empty list (`[{"title": "", "content":"", "color":""}]`), announcements are rendered. One for each item in the list. All announcement elements in the dictionary are optional. However, you probably want to have at least `content` to provide a meaningful message. |
| `with_artwork` | `false` | `main` | When the value is `"path/to/image.png"`, the page artwork is rendered. In this example, `path/to/image.png` must be relative to `/assets/img/` to display the image correctly. When the value is `false` the page artwork is not rendered.|
| `with_breadcrumbs` | `true` | `main` | When the value is `true`, the page breadcrumbs are rendered. When the value is `false` the page breadcrumbs are not rendered. |
| `with_content` | `true` | `main` | When the value is `true`, the page content is rendered. When the value is `false` the page content is not rendered. |
| `with_copyright` | `true` | `footer` | When the value is `true`, the page copyright is rendered. When the value is `false` the page copyright is not rendered. |
| `with_datatables` | `false` | `footer` | When the value is `true`, the datatables library to allow searching inside tables is enabled. When the value is `false` the datatables library is not enabled. |
| `with_finale` | `true` | `footer` | When the value is `true`, the page finale (i.e., site title plus site description) is rendered. When the value is `false` the page finale is not rendered.|
| `with_highlight` | `"default"` | `footer` | When the value is `true`, the highlight.js library for code highlighting is enabled. When the value is `false` the highlight.js library is not enabled. |
| `with_logo` | `"centos-whitelogo.svg"` | `nav` | Path to site's logo image in SVG format. The path is relative to `{% raw %}{{ site.url }}{{ site.baseurl }}/assets/img/{% endraw %}`. |
| `with_manifestation` | `""` | `nav` | When the value is an empty string (`""`), the manifestation is not rendered aside the logo. When the value is a `"string"`, that string is considered to be the manifestation name and is rendered aside the logo. |
| `with_motif` | `"centos-motif.png"` | `header` | Path to site's artistic motif in PNG format. The path is relative to `{% raw %}{{ site.url }}{{ site.baseurl }}/assets/img/{% endraw %}`. |
| `with_preamble` | `true` | `header` | When the value is `true`, the page preamble is rendered. When the value is `false` the page preamble is not rendered. |
| `with_shortcuts` | `true` | `footer` | When the value is `true`, the page shortcuts are rendered. When the value is `false` the page shortcuts are not rendered. |
| `with_social` | `true` | `footer` | When the value is `true`, the page social media are rendered. When the value is `false` the page social media are not rendered. |
| `with_sponsors` | `true` | `footer` | When the value is `true`, the page sponsors are rendered. When the value is `false` the page sponsors are not rendered. |
| `with_title` | `true` | `header` | When the value is `true`, the page title is rendered. When the value is `false` the page title is not rendered. |
| `with_toc` | `false` | `main` | When the value is `true`, the page table of content are rendered. When the value is `false` the page table of content is not rendered. |

The page variable default values are configured in the `defaults` site
variable. To know more about site defaults, see [the site configuration file](#the-site-configuration-file).

The page variable default values are overwritten when you add them in the page
front matter and change its value there.

The `base/default` layout also recognizes [Jekyll built-in page variables](https://jekyllrb.com/docs/variables/#page-variables).
