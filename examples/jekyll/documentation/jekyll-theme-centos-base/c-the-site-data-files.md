### The site data files

The `base/default` layout uses [data
files](https://jekyllrb.com/docs/datafiles/) written in YAML format. Each data
file has its own structure, serves its own purpose and is distributed read-only
inside the `jekyll-theme-centos` theme.

The `base/default` layout reads information from the following data files:

{:.table .table-bordered}
| Data file | Description |
| --------------------------------- | ----------- |
| `src/_data/base/copyright.yml` | Configure the site copyright information. |
| `src/_data/base/navbar.yml` | Configure the site navigation bar. The path to this specific data file can change based on the `component_data_dirname` and `component_data_navbar` site configuration variables. See [the site navigation bar data files](#the-site-navigation-bar-data-files). |
| `src/_data/base/social.yml` | Configure the site social media information. |

These data files do not exist in [the site directory
structure](#the-site-directory-structure) when you create it. Instead, they are
read from the theme directory structure when you run the task of [regenerating the
site public directory](#regenerating-the-site-public-directory). This approach is
very useful when you want to share re-usable information across different
websites but preserve a single point of edition for them. The most notable
examples are data files holding site copyright and site social media.

The copyright and social media data files must not exist in your site directory
structure, so you always use their default values from the theme directory
structure, where they are maintained. By doing so, all sites present the same
information consistently with minimum maintenance effort. This is good for
CentOS related sites but you might need to alter the rule when they are not.

In contrast to copyright and social media data files, the navigation bar data
file is expected to be changed locally, in your site directory structure. This
is because different sites have different content and navigation needs. When
you create a data files in your site directory, it must meet the exact same
YAML structure the theme provides. This convention allows the `base/default`
layout to find the information you provide and present it as expected.
