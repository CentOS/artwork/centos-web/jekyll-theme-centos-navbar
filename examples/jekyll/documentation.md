---
title: Documentation
title_lead: |
  This page describes the jekyll-theme-centos-base component and how you can
  use it in your sites.
categories: ["documentation", "jekyll-theme-centos-base"]

with_toc: true
with_highlight: stackoverflow-light
with_datatables: true
---

## Site Concepts

{% include_relative documentation/jekyll-theme-centos-base/c-the-site-directory-structure.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-configuration-file.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-configuration-variables.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-configuration-scopes.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-colors.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-fonts.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-data-files.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-navigation-bar.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-navigation-flow.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-navigation-breadcrumbs.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-social-networks.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-site-copyright-note.md %}

## Page Concepts

{% include_relative documentation/jekyll-theme-centos-base/c-the-page-base-default-layout.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-page-configuration-variables.md %}

{% include_relative documentation/jekyll-theme-centos-base/c-the-page-breakingnews.md %}
{% include_relative documentation/jekyll-theme-centos-base/c-the-page-announcements.md %}

## Site Tasks

This section describes frequent administration actions you perform in your
site.

{% include_relative documentation/jekyll-theme-centos-base/t-regenerating-the-site-public-directory.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-adding-the-site-announcements.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-adding-the-site-breakingnews.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-changing-the-site-artistic-motif.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-changing-the-site-favicon.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-changing-the-site-logo.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-changing-the-site-navigation-bar.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-changing-the-site-sass-variables.md %}

## Page Tasks

{% include_relative documentation/jekyll-theme-centos-base/t-adding-contextual-feedback.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-adding-the-page-announcements.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-adding-links-to-site-navigation-bar.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-adding-menus-to-site-navigation-bar.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-highlighting-pre-formatted-code.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-adding-tables.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-adding-tables-with-search-box.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-adding-images.md %}
{% include_relative documentation/jekyll-theme-centos-base/t-referencing-images.md %}

## References

This documentation tries to follow [The DITA Style Guide Best Practices for Authors](https://www.oxygenxml.com/dita/styleguide/).
