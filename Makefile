all:

.PHONY: install-sources
install-sources:
	# -------------------------------------------------------------------------------
	# Includes
	# -------------------------------------------------------------------------------
	install -D -t  examples/jekyll/_includes/base src/HTML/jekyll/_includes/*
	# -------------------------------------------------------------------------------
	# Layouts
	# -------------------------------------------------------------------------------
	install -D -t  examples/jekyll/_layouts/base src/HTML/jekyll/_layouts/*
	# -------------------------------------------------------------------------------
	# Data
	# -------------------------------------------------------------------------------
	install -D -t  examples/jekyll/_data/base src/YAML/*
	# -------------------------------------------------------------------------------
	# Images
	# -------------------------------------------------------------------------------
	install -D -t  examples/jekyll/assets/img/base/ src/IMG/*
	# -------------------------------------------------------------------------------
	# SASS
	# -------------------------------------------------------------------------------
	install -D -t  examples/jekyll/_sass/base/ src/SASS/*
	# -------------------------------------------------------------------------------
	# CSS
	# -------------------------------------------------------------------------------
	install -D -t  examples/jekyll/assets/css/base/ src/CSS/*

public: install-sources
	# -------------------------------------------------------------------------------
	# Public
	# -------------------------------------------------------------------------------
	install -d public
	# -------------------------------------------------------------------------------
	# Run server using jekyll-theme-centos:latest container image
	# -------------------------------------------------------------------------------
	podman run --rm \
		-v $$PWD/public:/public \
		-v $$PWD/examples/jekyll:/site \
		--name jekyll-theme-centos \
		registry.gitlab.com/centos/artwork/centos-web/jekyll-theme-centos:latest \
		bundle exec jekyll build --config /site/_config.yml -s /site -d /public

public-ansible:
	install -d public-ansible
	ansible-playbook -i examples/ansible/inventory -e public_dir=$$PWD/public-ansible -e component_name=jekyll-theme-centos-base -e component_dir=base examples/ansible/playbook.yml

html-differences: public public-ansible
	npx prettier --ignore-path .prettierignore -w public -w public-ansible
	npx html-validate public/ public-ansible/
	diff -s public*/index.html

.PHONY: jekyll-server
jekyll-server: clean install-sources
	# -------------------------------------------------------------------------------
	# Public
	# -------------------------------------------------------------------------------
	install -d public
	# -------------------------------------------------------------------------------
	# Run server using jekyll-theme-centos:latest container image
	# -------------------------------------------------------------------------------
	podman run --rm \
		-v $$PWD/public:/public \
		-v $$PWD/examples/jekyll:/site \
		-p 0.0.0.0:4000:4000 \
		--name jekyll-theme-centos \
		registry.gitlab.com/centos/artwork/centos-web/jekyll-theme-centos:latest \
		bundle exec jekyll serve -H 0.0.0.0 -p 4000 --config /site/_config.yml -s /site -d /public

.PHONY: clean
clean:
	$(RM) -r examples/jekyll/_includes
	$(RM) -r examples/jekyll/_layouts
	$(RM) -r examples/jekyll/_data
	$(RM) -r examples/jekyll/_sass
	$(RM) -r examples/jekyll/assets
	$(RM) -r examples/ansible/files
	$(RM) -r public
	$(RM) -r public-ansible
