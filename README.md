# jekyll-theme-centos-base

This project provides the base components of
[jekyll-theme-centos](https://gitlab.com/CentOS/artwork/centos-web/jekyll-theme-centos-base)
theme. Including HTML, SASS, and YAML definitions necessary to build pages
head, navbar, header, footer, and other common elements.

## License

[MIT License](https://opensource.org/licenses/MIT).
